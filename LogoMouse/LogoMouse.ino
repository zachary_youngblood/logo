#include <Servo.h>

#define RIGHT 1
#define LEFT -1

Servo leftServo;
Servo rightServo;

const byte ledPin = 13;
const byte buttonPin = 9;

const byte power = 250;

void forward()
{
    leftServo. writeMicroseconds(1500-power);
    rightServo.writeMicroseconds(1500+power);
}

void forwardTime(unsigned int time)
{
 forward();
delay(time);
}
void stop()
{  
    leftServo.writeMicroseconds(1500);
    rightServo.writeMicroseconds(1500);
}

void turn(int direction, int degrees)
{
  leftServo.writeMicroseconds(1500+power*direction);
  rightServo.writeMicroseconds(1500+power*direction);
  delay(degrees*5.6);
  stop();
}

void setup () 
{
    leftServo.attach(6);
    rightServo.attach(5);
    
    pinMode(ledPin,OUTPUT);
    pinMode(buttonPin,INPUT_PULLUP);

    while(digitalRead(buttonPin))
    {
    }

    turn(LEFT,180);
    turn(RIGHT,180);
}

void loop()
{
    digitalWrite(ledPin,HIGH);
}